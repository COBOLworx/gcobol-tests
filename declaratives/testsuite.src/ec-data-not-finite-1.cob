       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       01 OUR-RECORD.
          05 our-original PIC 9(2) VALUE ZERO. 
       01 our-division PIC 9(2).     
       PROCEDURE DIVISION.                                              

       DECLARATIVES.
       DECLARATIVE-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-DATA-NOT-FINITE.
              DISPLAY "DECLARATIVE FOR EC-DATA-NOT-FINITE".
           EXIT PROGRAM.       
       END DECLARATIVES.

       MAIN-SECTION SECTION.    
       DISPLAY "TURN EC-DATA-NOT-FINITE CHECKING ON".
       >>TURN EC-DATA-NOT-FINITE CHECKING ON
       PERFORM TEST1.
                                                                        
       EXIT PROGRAM.

       TEST1. 
           DIVIDE our-original INTO 0 GIVING our-division
           END-DIVIDE.
           DISPLAY our-division.

           END PROGRAM prog.
