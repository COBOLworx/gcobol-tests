       IDENTIFICATION DIVISION.
       PROGRAM-ID. prog.

       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01 return-value PIC X(6).

       PROCEDURE DIVISION.
           
           PERFORM TEST1.

       EXIT PROGRAM.

       TEST1.
           MOVE FUNCTION DATE-TO-YYYYMMDD 851003 120 TO return-value.

       END PROGRAM prog.
