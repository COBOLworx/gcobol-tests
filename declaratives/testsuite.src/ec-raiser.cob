        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog.

      *> Tests the RAISE EXCEPTION statement

        PROCEDURE DIVISION.

        DECLARATIVES.
        DECLARATIVE1 SECTION.
            USE AFTER EXCEPTION CONDITION EC-ARGUMENT-FUNCTION.
                DISPLAY   "               EC-ARGUMENT-FUNCTION".
                RESUME NEXT STATEMENT.
        DECLARATIVE2 SECTION.
            USE AFTER EXCEPTION CONDITION EC-SORT-MERGE-FILE-OPEN.
                DISPLAY   "               EC-BOUND-SUBSCRIPT".
                RESUME NEXT STATEMENT.
        DECLARATIVE3 SECTION.
            USE AFTER EXCEPTION CONDITION EC-BOUND-SUBSCRIPT.
                DISPLAY   "               EC-BOUND-SUBSCRIPT".
                RESUME NEXT STATEMENT.
        DECLARATIVE4 SECTION.
            USE AFTER EXCEPTION CONDITION EC-BOUND-REF-MOD.
                DISPLAY   "               EC-BOUND-REF-MOD".
                RESUME NEXT STATEMENT.
        DECLARATIVE5 SECTION.
            USE AFTER EXCEPTION CONDITION EC-BOUND-ODO.
                DISPLAY   "               EC-BOUND-ODO".
                RESUME NEXT STATEMENT.
        END DECLARATIVES.
        REQUIRED-SECTION SECTION.

            >>TURN EC-ALL CHECKING ON

            DISPLAY ""
            DISPLAY "About to raise EC-ARGUMENT-FUNCTION"
            RAISE EXCEPTION         EC-ARGUMENT-FUNCTION

            DISPLAY ""
            DISPLAY "About to raise EC-SORT-MERGE-FILE-OPEN"
            RAISE EXCEPTION         EC-SORT-MERGE-FILE-OPEN

            DISPLAY ""
            DISPLAY "About to raise EC-BOUND-SUBSCRIPT"
            RAISE EXCEPTION         EC-BOUND-SUBSCRIPT

            DISPLAY ""
            DISPLAY "About to raise EC-BOUND-REF-MOD"
            RAISE EXCEPTION         EC-BOUND-REF-MOD

            DISPLAY ""
            DISPLAY "About to raise EC-BOUND-ODO"
            RAISE EXCEPTION         EC-BOUND-ODO
