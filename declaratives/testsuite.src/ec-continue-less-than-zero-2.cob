       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       01 step-1        PIC X(2).
       01 time-variable PIC 9(2)    VALUE -1.
       01 step-2        PIC X(2).

       PROCEDURE DIVISION.                                              
       DECLARATIVES.
       DECLARATIVE-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-CONTINUE.
              DISPLAY "DECLARATIVE FOR EC-CONTINUE".
           EXIT PROGRAM.       
       END DECLARATIVES.

       MAIN-SECTION SECTION.    
       DISPLAY "TURN EC-CONTINUE CHECKING ON".    
       >>TURN EC-CONTINUE CHECKING ON
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1. 
           MOVE 12 TO step-1.
           CONTINUE AFTER time-variable seconds.
           MOVE 34 TO step-2.

           END PROGRAM prog.
