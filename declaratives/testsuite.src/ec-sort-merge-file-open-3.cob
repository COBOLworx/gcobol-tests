       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       
       PROCEDURE DIVISION.                                              

       DECLARATIVES.
       DECLARATIVE-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-ALL.
              DISPLAY "DECLARATIVE FOR EC-ALL".
           EXIT PROGRAM.
       END DECLARATIVES.

       MAIN-SECTION SECTION.    
       DISPLAY "TURN EC-ALL CHECKING ON".    
       >>TURN EC-ALL CHECKING ON
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1. 
