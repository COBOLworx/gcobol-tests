       IDENTIFICATION DIVISION.
       PROGRAM-ID. prog.

       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01 variables.
               05 first-number      PIC 99 VALUE 10.
               05 second-number     PIC 99 VALUE 00.

       PROCEDURE DIVISION.
       DECLARATIVES.
       DECLARATIVES-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-SIZE.
             DISPLAY "DECLARATIVE FOR EC-SIZE".
           EXIT PROGRAM.
       END DECLARATIVES.

       MAIN-SECTION SECTION. 
       DISPLAY "TURN EC-SIZE CHECKING ON".
       >>TURN EC-SIZE CHECKING ON
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1.
          DIVIDE first-number INTO second-number.

       END PROGRAM prog.
