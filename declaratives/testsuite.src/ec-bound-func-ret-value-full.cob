       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       01 return-value PIC X(6).
       
       PROCEDURE DIVISION.                                              

       DECLARATIVES.
       DECLARATIVE-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-BOUND-FUNC-RET-VALUE.
              DISPLAY "DECLARATIVE FOR EC-BOUND-FUNC-RET-VALUE".
       DECLARATIVE-2 SECTION.
           USE AFTER EXCEPTION CONDITION EC-BOUND.
              DISPLAY "DECLARATIVE FOR EC-BOUND".
       DECLARATIVE-3 SECTION.
           USE AFTER EXCEPTION CONDITION EC-ALL.
              DISPLAY "DECLARATIVE FOR EC-ALL".

       END DECLARATIVES.

       MAIN-SECTION SECTION.    
       >>TURN EC-BOUND-FUNC-RET-VALUE CHECKING ON
       >>TURN EC-BOUND CHECKING OFF
       >>TURN EC-ALL CHECKING OFF    
       DISPLAY "TURN EC-BOUND-FUNC-RET-VALUE CHECKING ON".
       DISPLAY "TURN EC-BOUND CHECKING OFF".
       DISPLAY "TURN EC-ALL CHECKING OFF".
       PERFORM TEST1.
                                                                        
       >>TURN EC-BOUND-FUNC-RET-VALUE CHECKING OFF
       >>TURN EC-BOUND CHECKING ON
       DISPLAY "TURN EC-BOUND-FUNC-RET-VALUE CHECKING OFF".
       DISPLAY "TURN EC-BOUND CHECKING ON".    
       PERFORM TEST1.

       >>TURN EC-BOUND CHECKING OFF
       >>TURN EC-ALL CHECKING ON
       DISPLAY "TURN EC-BOUND CHECKING OFF".
       DISPLAY "TURN EC-ALL CHECKING ON".    
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1. 
           MOVE FUNCTION DATE-TO-YYYYMMDD TO return-value.
