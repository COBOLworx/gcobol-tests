       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       01 step-1        PIC X(2).
       01 time-variable PIC 9(2)    VALUE -1.
       01 step-2        PIC X(2).

       PROCEDURE DIVISION.                                              
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1. 
           MOVE 12 TO step-1.
           CONTINUE AFTER time-variable seconds.
           MOVE 34 TO step-2.

           END PROGRAM prog.
