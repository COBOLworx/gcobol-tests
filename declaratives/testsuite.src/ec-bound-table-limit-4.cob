       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       01 OUR-RECORD.
           05 our-table  OCCURS DYNAMIC CAPACITY IN data-3
                         FROM 5 TO 7 INITIALIZED.
              10 our-something  PIC X(2).

       01 our-index.
           05 index-1    PIC 9(2).
           05 data-3     PIC 9(2).

       PROCEDURE DIVISION.                                              
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1. 
         MOVE 3 TO our-something(8).

           END PROGRAM prog.
