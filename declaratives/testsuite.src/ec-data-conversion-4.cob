       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       01 OUR-RECORD.
           05 our-original  PIC 9(5) VALUE 12345.
       01 our-conversion    PIC 9(5) VALUE 67890.
       
       PROCEDURE DIVISION.                                              

       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1. 
           INSPECT our-original CONVERTING "67" TO our-conversion.

           END PROGRAM prog.
