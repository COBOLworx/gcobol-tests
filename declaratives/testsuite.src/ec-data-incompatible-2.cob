       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       01 OUR-RECORD.
         05 our-original PIC 9(2). 

       PROCEDURE DIVISION.                                              

       DECLARATIVES.
       DECLARATIVE-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-DATA.
              DISPLAY "DECLARATIVE FOR EC-DATA".
           EXIT PROGRAM.       
       END DECLARATIVES.

       MAIN-SECTION SECTION.    
       DISPLAY "TURN EC-DATA CHECKING ON".    
       >>TURN EC-DATA CHECKING ON
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1. 
           MOVE "XY" TO our-original.

           END PROGRAM prog.
