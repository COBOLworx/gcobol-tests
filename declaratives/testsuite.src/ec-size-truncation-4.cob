       IDENTIFICATION DIVISION.
       PROGRAM-ID. prog.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       
       01   VAR3     PIC 999.
           
       PROCEDURE DIVISION.

       MAIN-SECTION SECTION. 
           
           PERFORM TEST1.

       EXIT PROGRAM.

       TEST1.
           COMPUTE VAR3 = 1000.

       END PROGRAM prog.
