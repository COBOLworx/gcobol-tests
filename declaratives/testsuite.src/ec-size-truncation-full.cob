       IDENTIFICATION DIVISION.
       PROGRAM-ID. prog.

       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01   VAR3     PIC 999.

       PROCEDURE DIVISION.
       DECLARATIVES.
       DECLARATIVES-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-SIZE-TRUNCATION.
             DISPLAY "DECLARATIVE FOR EC-SIZE-TRUNCATION".
             RESUME NEXT STATEMENT.
        DECLARATIVES-2 SECTION.
           USE AFTER EXCEPTION CONDITION EC-SIZE.
             DISPLAY "DECLARATIVE FOR EC-SIZE".
             RESUME NEXT STATEMENT.
         DECLARATIVES-3 SECTION.
           USE AFTER EXCEPTION CONDITION EC-ALL.
             DISPLAY "DECLARATIVE FOR EC-ALL".
             RESUME NEXT STATEMENT.
          END DECLARATIVES.

       MAIN-SECTION SECTION. 
       DISPLAY "TURN EC-SIZE-TRUNCATION CHECKING ON".
       DISPLAY "TURN EC-SIZE CHECKING OFF".
       DISPLAY "TURN EC-ALL CHECKING OFF".
       >>TURN EC-SIZE-TRUNCATION CHECKING ON
       >>TURN EC-SIZE CHECKING OFF
       >>TURN EC-ALL CHECKING OFF
       PERFORM TEST1.

       DISPLAY "TURN EC-SIZE-TRUNCATION CHECKING OFF".
       DISPLAY "TURN EC-SIZE CHECKING ON".
       >>TURN EC-SIZE-TRUNCATION CHECKING OFF
       >>TURN EC-SIZE CHECKING ON
       PERFORM TEST1.

       DISPLAY "TURN EC-SIZE CHECKING OFF".
       DISPLAY "TURN EC-ALL CHECKING ON".
       >>TURN EC-SIZE CHECKING OFF
       >>TURN EC-ALL CHECKING OFF
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1.
           COMPUTE VAR3 = 1000.

       END PROGRAM prog.
