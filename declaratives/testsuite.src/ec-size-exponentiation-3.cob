       IDENTIFICATION DIVISION.
       PROGRAM-ID. prog.

       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01 variable-list.
               05 variable-1     PIC 9    VALUE 0.

       PROCEDURE DIVISION.
       DECLARATIVES.
       DECLARATIVES-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-ALL.
             DISPLAY "DECLARATIVE FOR EC-ALL".
           EXIT PROGRAM.           
       END DECLARATIVES.

       MAIN-SECTION SECTION. 
       DISPLAY "TURN EC-ALL CHECKING ON".
       >>TURN EC-ALL CHECKING ON
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1.
           COMPUTE (variable-1)^12.

       END PROGRAM prog.
