       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       01 OUR-RECORD.
           05 our-table  OCCURS DYNAMIC CAPACITY IN data-3
                         FROM 5 TO 7 INITIALIZED.
              10 our-something  PIC X(2).

       01 our-index.
           05 index-1    PIC 9(2).
           05 data-3     PIC 9(2).

       PROCEDURE DIVISION.                                              

       DECLARATIVES.
       DECLARATIVE-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-BOUND-TABLE-LIMIT.
              DISPLAY "DECLARATIVE FOR EC-BOUND-TABLE-LIMIT".
       DECLARATIVE-2 SECTION.
           USE AFTER EXCEPTION CONDITION EC-BOUND.
              DISPLAY "DECLARATIVE FOR EC-BOUND".
       DECLARATIVE-3 SECTION.
           USE AFTER EXCEPTION CONDITION EC-ALL.
              DISPLAY "DECLARATIVE FOR EC-ALL".

       END DECLARATIVES.

       MAIN-SECTION SECTION.    
       >>TURN EC-BOUND-TABLE-LIMIT CHECKING ON
       >>TURN EC-BOUND CHECKING OFF
       >>TURN EC-ALL CHECKING OFF    
       DISPLAY "TURN EC-BOUND-TABLE-LIMIT CHECKING ON".
       DISPLAY "TURN EC-BOUND CHECKING OFF".
       DISPLAY "TURN EC-ALL CHECKING OFF".
       PERFORM TEST1.
                                                                        
       >>TURN EC-BOUND-TABLE-LIMIT CHECKING OFF
       >>TURN EC-BOUND CHECKING ON
       DISPLAY "TURN EC-BOUND-TABLE-LIMIT CHECKING OFF".
       DISPLAY "TURN EC-BOUND CHECKING ON".    
       PERFORM TEST1.

       >>TURN EC-BOUND CHECKING OFF
       >>TURN EC-ALL CHECKING ON
       DISPLAY "TURN EC-BOUND CHECKING OFF".
       DISPLAY "TURN EC-ALL CHECKING ON".    
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1. 
         MOVE 3 TO our-something(8).
