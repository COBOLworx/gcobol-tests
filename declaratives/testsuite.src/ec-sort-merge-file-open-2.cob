       IDENTIFICATION DIVISION.

       PROGRAM-ID. prog.

       DATA DIVISION.

       WORKING-STORAGE SECTION.                                         
       
       PROCEDURE DIVISION.                                              

       DECLARATIVES.
       DECLARATIVE-1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-SORT.
              DISPLAY "DECLARATIVE FOR EC-SORT".
           EXIT PROGRAM.
       END DECLARATIVES.

       MAIN-SECTION SECTION.    
       DISPLAY "TURN EC-SORT CHECKING ON".    
       >>TURN EC-SORT CHECKING ON
       PERFORM TEST1.

       EXIT PROGRAM.

       TEST1. 
