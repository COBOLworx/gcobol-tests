       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. prog.                                                      
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 our-record.
          05 our-table occurs 4 times pic x.
       77 our-index pic 99 value 5.
                                                                        
       PROCEDURE DIVISION.                                              
       DECLARATIVES.
       DECLARATIVE1 SECTION.
           USE AFTER EXCEPTION CONDITION EC-ALL.
              DISPLAY "DECLARATIVE FOR EC-BOUND".
              DISPLAY FUNCTION EXCEPTION-LOCATION.
              DISPLAY FUNCTION EXCEPTION-STATEMENT.
              DISPLAY FUNCTION EXCEPTION-STATUS.
       END DECLARATIVES.

       >>TURN EC-ALL CHECKING ON

       .

       test-1.
       move 3 to our-table(our-index).

