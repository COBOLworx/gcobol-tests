        IDENTIFICATION DIVISION.
        PROGRAM-ID. one-ring.
        PROCEDURE DIVISION.
        CALL "prog-subscript"
        CALL "prog-bound"
        CALL "prog-ec"
        GOBACK.
        END PROGRAM one-ring.

        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog-subscript.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 OUR-RECORD.
           05 filler        OCCURS 4 TIMES PIC X(5).
           05 our-table     OCCURS 4 TIMES PIC X(5).
           05 filler        OCCURS 4 TIMES PIC X(5).
        01 our-index pic 99 value 6.
        01 MSG0 PIC X(64) VALUE "Nothing".
        01 MSG1 PIC X(64) VALUE "EC-BOUND-SUBSCRIPT".
        01 MSG2 PIC X(64) VALUE "EC-BOUND".
        01 MSG3 PIC X(64) VALUE "EC-ALL".
        01 SHOULD-BE PIc X(64).
        01 BUT-IS    PIC X(64).
        PROCEDURE DIVISION.
        DECLARATIVES.
        DECLARATIVE1 SECTION.
            USE AFTER EXCEPTION CONDITION EC-BOUND-SUBSCRIPT.
                DISPLAY "DECLARATIVE FOR EC-BOUND-SUBSCRIPT".
                MOVE MSG1 TO BUT-IS.
                RESUME NEXT STATEMENT.
        DECLARATIVE2 SECTION.
            USE AFTER EXCEPTION CONDITION EC-BOUND.
                MOVE MSG2 TO BUT-IS.
                DISPLAY "DECLARATIVE FOR EC-BOUND".
                RESUME NEXT STATEMENT.
        DECLARATIVE3 SECTION.
            USE AFTER EXCEPTION CONDITION EC-ALL.
                MOVE MSG3 TO BUT-IS.
                DISPLAY "DECLARATIVE FOR EC-ALL".
                RESUME NEXT STATEMENT.
           END DECLARATIVES.
        MAIN SECTION.
        MOVE MSG1 TO SHOULD-BE.
        MOVE MSG0 TO BUT-IS.
        PERFORM TEST1.
        PERFORM DCHECK.
        GOBACK.
        TEST1.
            >>TURN EC-ALL CHECKING ON
            display "BEGIN TEST1".
            MOVE "12345" TO our-table(our-index).
            >>TURN EC-ALL CHECKING OFF
            display "END   TEST1".
        DCHECK.
            if SHOULD-BE EQUAL BUT-IS
                DISPLAY "      The result should be " FUNCTION TRIM(SHOULD-BE) " and is " FUNCTION TRIM(BUT-IS)
            ELSE
                DISPLAY "!!!!! The result should be " FUNCTION TRIM(SHOULD-BE) " but is " FUNCTION TRIM(BUT-IS)
            END-IF.
        END PROGRAM prog-subscript.

        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog-bound.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 OUR-RECORD.
           05 filler        OCCURS 4 TIMES PIC X(5).
           05 our-table     OCCURS 4 TIMES PIC X(5).
           05 filler        OCCURS 4 TIMES PIC X(5).
        01 our-index pic 99 value 6.
        01 MSG0 PIC X(64) VALUE "Nothing".
        01 MSG1 PIC X(64) VALUE "EC-BOUND-SUBSCRIPT".
        01 MSG2 PIC X(64) VALUE "EC-BOUND".
        01 MSG3 PIC X(64) VALUE "EC-ALL".
        01 SHOULD-BE PIc X(64).
        01 BUT-IS    PIC X(64).
        PROCEDURE DIVISION.
        DECLARATIVES.
        DECLARATIVE2 SECTION.
            USE AFTER EXCEPTION CONDITION EC-BOUND.
                MOVE MSG2 TO BUT-IS.
                DISPLAY "DECLARATIVE FOR EC-BOUND".
                RESUME NEXT STATEMENT.
        DECLARATIVE3 SECTION.
            USE AFTER EXCEPTION CONDITION EC-ALL.
                MOVE MSG3 TO BUT-IS.
                DISPLAY "DECLARATIVE FOR EC-ALL".
                RESUME NEXT STATEMENT.
        DECLARATIVE1 SECTION.
            USE AFTER EXCEPTION CONDITION EC-BOUND-SUBSCRIPT.
                DISPLAY "DECLARATIVE FOR EC-BOUND-SUBSCRIPT".
                MOVE MSG1 TO BUT-IS.
                RESUME NEXT STATEMENT.
           END DECLARATIVES.
        MAIN SECTION.
        MOVE MSG2 TO SHOULD-BE.
        MOVE MSG0 TO BUT-IS.
        PERFORM TEST1.
        PERFORM DCHECK.
        GOBACK.
        TEST1.
            >>TURN EC-ALL CHECKING ON
            display "BEGIN TEST1".
            MOVE "12345" TO our-table(our-index).
            >>TURN EC-ALL CHECKING OFF
            display "END   TEST1".
        DCHECK.
            if SHOULD-BE EQUAL BUT-IS
                DISPLAY "      The result should be " FUNCTION TRIM(SHOULD-BE) " and is " FUNCTION TRIM(BUT-IS)
            ELSE
                DISPLAY "!!!!! The result should be " FUNCTION TRIM(SHOULD-BE) " but is " FUNCTION TRIM(BUT-IS)
            END-IF.
        END PROGRAM prog-bound.

        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog-ec.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 OUR-RECORD.
           05 filler        OCCURS 4 TIMES PIC X(5).
           05 our-table     OCCURS 4 TIMES PIC X(5).
           05 filler        OCCURS 4 TIMES PIC X(5).
        01 our-index pic 99 value 6.
        01 MSG0 PIC X(64) VALUE "Nothing".
        01 MSG1 PIC X(64) VALUE "EC-BOUND-SUBSCRIPT".
        01 MSG2 PIC X(64) VALUE "EC-BOUND".
        01 MSG3 PIC X(64) VALUE "EC-ALL".
        01 SHOULD-BE PIc X(64).
        01 BUT-IS    PIC X(64).
        PROCEDURE DIVISION.
        DECLARATIVES.
        DECLARATIVE3 SECTION.
            USE AFTER EXCEPTION CONDITION EC-ALL.
                MOVE MSG3 TO BUT-IS.
                DISPLAY "DECLARATIVE FOR EC-ALL".
                DISPLAY "      Location:  " FUNCTION EXCEPTION-LOCATION
                DISPLAY "      Statement: " FUNCTION EXCEPTION-STATEMENT
                DISPLAY "      Status:    " FUNCTION EXCEPTION-STATUS
                RESUME NEXT STATEMENT.
        DECLARATIVE1 SECTION.
            USE AFTER EXCEPTION CONDITION EC-BOUND-SUBSCRIPT.
                DISPLAY "DECLARATIVE FOR EC-BOUND-SUBSCRIPT".
                MOVE MSG1 TO BUT-IS.
                RESUME NEXT STATEMENT.
        DECLARATIVE2 SECTION.
            USE AFTER EXCEPTION CONDITION EC-BOUND.
                MOVE MSG2 TO BUT-IS.
                DISPLAY "DECLARATIVE FOR EC-BOUND".
                RESUME NEXT STATEMENT.
           END DECLARATIVES.
        MAIN SECTION.
        MOVE MSG3 TO SHOULD-BE.
        MOVE MSG0 TO BUT-IS.
        PERFORM TEST1.
        PERFORM DCHECK.
        GOBACK.
        TEST1-SECTION SECTION.
        TEST1.
            >>TURN EC-ALL CHECKING ON
            display "BEGIN TEST1".
            MOVE "12345" TO our-table(our-index).
            >>TURN EC-ALL CHECKING OFF
            display "END   TEST1".
        DCHECK.
            if SHOULD-BE EQUAL BUT-IS
                DISPLAY "      The result should be " FUNCTION TRIM(SHOULD-BE) " and is " FUNCTION TRIM(BUT-IS)
            ELSE
                DISPLAY "!!!!! The result should be " FUNCTION TRIM(SHOULD-BE) " but is " FUNCTION TRIM(BUT-IS)
            END-IF.
        END PROGRAM prog-ec.

