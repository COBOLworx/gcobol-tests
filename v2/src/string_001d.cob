      *> string001d
      *> string edited numeric to string
       ID DIVISION.
       PROGRAM-ID. prog.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 var-1 PIC X(10) VALUE SPACE. 
       77 var-2 PIC 999.99 VALUE 122.3. 
       PROCEDURE DIVISION.
          STRING var-2 INTO var-1.
           IF var-1(1:1) = "+" THEN
             MOVE var-1(2:) TO var-1
             END-IF.
           IF "122.30" NOT = var-1 THEN
             DISPLAY var-1
             MOVE 1 TO RETURN-CODE
             END-IF.
       END PROGRAM prog.
       
