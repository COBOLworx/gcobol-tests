       ID DIVISION.

       PROGRAM-ID. work-test1.

       ENVIRONMENT DIVISION.
           
       INPUT-OUTPUT SECTION.

         SELECT rlog-file ASSIGN TO "nick-data.rlog"
                FILE STATUS IS rlog-status
                ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION.
       
       FILE SECTION.
       
         FD rlog-file.
       
         01 rlog-data             PIC X(80).
       
       WORKING-STORAGE SECTION.
       
         01 rlog-record.
               05 start-line      PIC 9(7).
               05 connection      PIC 9(7).
               05 session         PIC 9(3).
               05 verb            PIC X(10).
               05 tag             PIC 9(5).
               05 return-code     PIC 9(2).
               05 num-entries     PIC 9(7).
               05 queue-time      PIC 9(3)V9(6).
               05 elapsed-time    PIC 9(3)V9(6).
               05 first-instance  PIC 9(3)V9(6).
               05 last-instance   PIC 9(3)V9(6).
               05 ending-line     PIC 9(7).            

         01 random-stuff.
               05 line-counter    PIC 9(6)        USAGE binary.
               05 rlog-status     PIC 9(2).
       
       PROCEDURE DIVISION.
       
           OPEN INPUT rlog-file.
           
           IF rlog-status IS GREATER THAN 10 THEN
                 DISPLAY "Ooops. Input file IO problem: "
                 rlog-status
           END-IF.
           
           READ rlog-file.
           
           DISPLAY rlog-data.
           
           EXIT program.

