       ID DIVISION.

       PROGRAM-ID. work-test1.

       ENVIRONMENT DIVISION.
           
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

         SELECT rlog-file ASSIGN TO "nick-data.rlog"
                FILE STATUS IS rlog-status
                ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION.
       
       FILE SECTION.
       
         FD rlog-file.
       
         01 rlog-data             PIC X(80).
       
       WORKING-STORAGE SECTION.
       
         01 rlog-record.
               05 start-line      PIC 9(7).
               05 connection      PIC 9(7).
               05 operation       pic 9(4).       
               05 session         PIC 9(3).
               05 verb            PIC X(10).
               05 tag             PIC 9(5).
               05 rtn-code        PIC 9(3).
               05 num-entries     PIC 9(7).
               05 queue-time      PIC 9(3)V9(6).
               05 elapsed-time    PIC 9(3)V9(6).
               05 first-instance  PIC X(8).
               05 last-instance   PIC X(8).
               05 ending-line     PIC 9(7).            

         01 random-stuff.
               05 line-count      PIC 9(6)        USAGE binary.
               05 rlog-status     PIC 9(2).
               05 verb-count      PIC 9(4).
               05 tag-count       PIC 9(4). 
       
       PROCEDURE DIVISION.
       
           OPEN INPUT rlog-file.
           
           IF rlog-status IS GREATER THAN 10 THEN
                 DISPLAY "Ooops. Input file IO problem: "
                 rlog-status
           END-IF.
           
           PERFORM UNTIL rlog-status >= 10
                   READ rlog-file INTO rlog-record 
                        AT END PERFORM all-done
                   END-READ
                   DISPLAY rlog-status
                   
      D            IF line-count = 1 THEN
      D               DISPLAY rlog-record
      D            END-IF
                   
                   DISPLAY rlog-record(1:40)

           UNSTRING rlog-data DELIMITED BY SPACE 
                INTO    start-line
                        connection
                        operation
                        session
                        verb
                        tag
                        return-code
                        num-entries
                        queue-time
                        elapsed-time
                        first-instance
                        last-instance
                        ending-line
           END-UNSTRING

      D    DISPLAY "Verb: " verb OF rlog-record
      D    DISPLAY rlog-record
           IF verb OF rlog-record IS EQUAL TO "SRCH" THEN
                ADD 1 TO verb-count  
                END-IF

           IF tag OF rlog-record IS EQUAL TO 101 THEN
                ADD 1 TO tag-count
                END-IF             
           END-PERFORM.

       all-done.
           DISPLAY "RLOG-STATUS: " rlog-status *> "10"
           DISPLAY "Total SRCHs: "verb-count
           DISPLAY "Total 101 Tags: "tag-count
           CLOSE rlog-file.
           GOBACK
           EXIT program.


