.PHONY: all fail skip bugs

test:
	@echo "testsuite has been moved to gcc-cobol/gcc/cobol/UAT"

all:	bugs fail skip

fail:	failsuite
	./failsuite $(if $(KEYWORDS),-k $(KEYWORDS))

skip:	skipsuite
	./skipsuite $(if $(KEYWORDS),-k $(KEYWORDS))

bugs:	bugsuite
	./bugsuite $(if $(KEYWORDS),-k $(KEYWORDS))

FAILSUITE_AT_FILES=$(wildcard ./failsuite.src/*.at)
SKIPSUITE_AT_FILES=$(wildcard ./skipsuite.src/*.at)
BUGSUITE_AT_FILES=$(wildcard ./bugsuite.src/*.at)

failsuite: atlocal $(FAILSUITE_AT_FILES) failsuite.at
	autom4te --language=autotest -I $@.src -o $@ $@.at

skipsuite: atlocal $(SKIPSUITE_AT_FILES) skipsuite.at
	autom4te --language=autotest -I $@.src -o $@ $@.at

bugsuite: atlocal $(BUGSUITE_AT_FILES) bugsuite.at
	autom4te --language=autotest -I $@.src -o $@ $@.at

clean:
	@rm -fr *~ *.log failsuite skipsuite bugsuite \
                failsuite.dir skipsuite.dir bugsuite.dir
